import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { ApiService } from '.';``

@Injectable({
  providedIn: 'root'
})
export class PokemonService {
 
  selectedItem
  private pokemonTypes
  constructor(private api:ApiService, private storage: Storage) { }

  getPokemonList(pagination){
    return this.api.getData("pokemon",null,null, {offset:pagination.offset, limit:pagination.limit})
  }

  getPokemonListWithType(type){
    return this.api.getData(`type/${type}`)
  }

  getPokemonDetail(id){
    return this.api.getData(`pokemon/${id}`)
  }

  getPokemonHabitat(id){
    return this.api.getData(`pokemon-habitat/${id}`)
  }

  getPokemonEggGroup(id){
    return this.api.getData(`egg-group/${id}`)
  }

  getPokemonShape(id) {
    return this.api.getData(`pokemon-shape/${id}`)
  }

  storeFavoritesData(data){
    // Update Favorites data in Local Storage
    this.storage.set("favorites", data)
  }

  getFavoritesData(){
    // Get favorites Data from local storage
    return this.storage.get("favorites")
  }

  getPokemonType(){
    return this.api.getData(`type`)
  }

  getLocalPokemonType(){
    return this.pokemonTypes
  }
  setLocalPokemonTypes(value){
    this.pokemonTypes = value
  }
}
