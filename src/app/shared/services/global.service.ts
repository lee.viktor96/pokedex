import { Injectable, isDevMode } from '@angular/core';
import { ToastController } from '@ionic/angular';
import * as _ from 'lodash';


@Injectable({
  providedIn: 'root'
})
export class GlobalService {
  constructor(private toastController: ToastController) { }

  log(message: string, data: any = null, type: string = 'log') {
    if(isDevMode()){
      if(type === 'log'){
        if(data){
          console.log(message, data)
        }else{
          console.log(message)
        }
      } else if(type === 'error') {
        console.error(message, data);
      }
    }
  }

  async showToast(message){
    const toast = await this.toastController.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

  
}
