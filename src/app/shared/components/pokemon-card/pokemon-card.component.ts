import { Component, Input, OnInit } from "@angular/core";
import { NavController } from "@ionic/angular";
import { PokemonService } from "@shared/services/pokemon.service";

@Component({
  selector: "app-pokemon-card",
  templateUrl: "./pokemon-card.component.html",
  styleUrls: ["./pokemon-card.component.scss"],
})
export class PokemonCardComponent implements OnInit {
  @Input("item") item;
  @Input("favoritesList") favoritesList;
  constructor(private pokemonSrv: PokemonService, private navCtrl: NavController) {}

  ngOnInit() {}

  manageFavorites(item) {
    if (this.favoritesList[item.stringId]) {
      delete this.favoritesList[item.stringId];
    } else {
      this.favoritesList[item.stringId] = item;
    }
    this.pokemonSrv.storeFavoritesData(this.favoritesList);
  }

  toDetail() {
    this.navCtrl.navigateForward(`/detail/${this.item.detail.id}`);
  }
}
