import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { NavController, Platform } from "@ionic/angular";
import { GlobalService } from "@shared/services";
import { PokemonService } from "@shared/services/pokemon.service";

@Component({
  selector: "app-detail",
  templateUrl: "./detail.page.html",
  styleUrls: ["./detail.page.scss"],
})
export class DetailPage implements OnInit {
  pokemonData
  selectedSegment = "about";
  favoritesList = {}
  constructor(
    private pokemonSrv: PokemonService,
    private activatedRoute: ActivatedRoute,
    private navCtrl: NavController,
    private gs:GlobalService
  ) {
    this.activatedRoute.params.subscribe((data) => {
      if (data["id"]) {
        this.initData(data["id"]);
      }
    });
  }

  ngOnInit() {}

  async initData(id) {
    this.pokemonData = {}
    this.pokemonData["stringId"] = id.toString().padStart(3, "0");

    this.pokemonSrv.getFavoritesData().then(data=>{
      if(data){
        this.favoritesList = data
      }
    })

    try{
      this.pokemonData["detail"] = await this.pokemonSrv.getPokemonDetail(id).toPromise()
      this.pokemonData["habitat"] = await this.pokemonSrv.getPokemonHabitat(id).toPromise()
      this.pokemonData["egggroup"] = await this.pokemonSrv.getPokemonEggGroup(id).toPromise()
      this.pokemonData["shape"] = await this.pokemonSrv.getPokemonShape(id).toPromise()
    }catch(err){
      this.gs.showToast("Something Went Wrong")
    }
  }

  back() {
    this.navCtrl.back();
  }

  manageFavorites(item){
    if (this.favoritesList[item.stringId]) {
      delete this.favoritesList[item.stringId];
    } else {
      this.favoritesList[item.stringId] = item;
    }
    this.pokemonSrv.storeFavoritesData(this.favoritesList)
  }
}
