import { Component, OnInit } from "@angular/core";
import { NavController, Platform } from "@ionic/angular";
import { PokemonService } from "@shared/services/pokemon.service";

@Component({
  selector: "app-favorites",
  templateUrl: "./favorites.page.html",
  styleUrls: ["./favorites.page.scss"],
})
export class FavoritesPage implements OnInit {
  favoritesList = {};
  objectkeys = Object.keys;

  constructor(private pokemonSrv: PokemonService) {
    this.initData();
  }

  initData() {
    this.pokemonSrv.getFavoritesData().then((data) => {
      if (data) {
        this.favoritesList = data;
      }
    });
  }

  ngOnInit() {}
}
