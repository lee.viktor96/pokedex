import { Component, OnInit } from "@angular/core";
import { ModalController, Platform } from "@ionic/angular";
import { ModalFilterComponent } from "src/app/pages/home/modal-filter/modal-filter.component";
import { PokemonService } from "@shared/services/pokemon.service";
import { GlobalService } from "@shared/services";

@Component({
  selector: "app-home",
  templateUrl: "home.page.html",
  styleUrls: ["home.page.scss"],
})
export class HomePage implements OnInit {
  filterBtn: any;

  pagination = {
    offset: 0,
    limit: 20,
    isReachEnd: false,
  };

  pokemonList;
  favoritesList = {};

  currentFilter;
  constructor(private pokemonSrv: PokemonService, private modalController: ModalController, private gs: GlobalService) {
    this.initData();
  }

  initData() {
    this.pokemonSrv.getPokemonList(this.pagination).subscribe(
      (res) => {
        const tempPokemonData = res.results;
        tempPokemonData.forEach((pokemon) => {
          // Split from URL to get pokemon ID
          const urlSplit = pokemon.url.split("/");
          const id = urlSplit[urlSplit.length - 2];
          // String ID : 001, for favorites
          pokemon.stringId = id.toString().padStart(3, "0");
          this.pokemonSrv.getPokemonDetail(id).subscribe((resDetail) => {
            pokemon.detail = resDetail;
          });
        });
        if(!this.pokemonList) this.pokemonList = []
        this.pokemonList = this.pokemonList.concat(tempPokemonData);
        if (this.pokemonList.length > res.count) {
          this.pagination.isReachEnd = true;
        }
      },
      (err) => {
        this.gs.showToast("Something went wrong");
        this.pokemonList = [];
      }
    );
  }

  fetchMore(event) {
    this.pagination.offset += 20;
    this.initData();
    setTimeout(() => {
      event.target.complete();
    }, 1500);
  }

  ngOnInit() {}

  initFavoritesData() {
    this.pokemonSrv.getFavoritesData().then((data) => {
      if (data) {
        this.favoritesList = data;
      }
    });
  }
  backSubscription;
  ionViewWillEnter() {
    this.initFavoritesData();
  }

  async showModalFilter() {
    const modal = await this.modalController.create({
      component: ModalFilterComponent,
      cssClass: "filter-modal",
      componentProps: { currentFilter: this.currentFilter },
    });

    await modal.present();
    modal.onDidDismiss().then((res) => {
      if (res.data) {
        this.pagination.offset = 0;
        this.currentFilter = res.data;
        this.pokemonList = [];
      } else {
        this.currentFilter = null;
        this.initData();
      }
    });
  }
}
