import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { HomePage } from './home.page';
import { PokemonCardModule } from '@shared/components/pokemon-card/pokemon-card.module';
import { TypeListComponent } from './type-list/type-list.component';
import { ModalFilterComponent } from './modal-filter/modal-filter.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomePage
      }
    ]),
    PokemonCardModule
  ],
  declarations: [HomePage, TypeListComponent, ModalFilterComponent],
  entryComponents : [ModalFilterComponent]
})
export class HomePageModule {}
