import { Component, Input, OnInit } from "@angular/core";
import { GlobalService } from "@shared/services";
import { PokemonService } from "@shared/services/pokemon.service";

@Component({
  selector: "app-type-list",
  templateUrl: "./type-list.component.html",
  styleUrls: ["./type-list.component.scss"],
})
export class TypeListComponent implements OnInit {
  @Input("currentFilter") set filter(value) {
    if (value) {
      // Init data when pokemon type filter is provided
      this.initDataWithFilter(value.name);
    }
  }
  @Input("favoritesList") favoritesList;

  pokemonList = [];
  masterPokemonList = [];

  pagination = {
    offset: 0,
    limit: 20,
  };

  constructor(private pokemonSrv: PokemonService, private gs:GlobalService) {}

  ngOnInit() {}

  initDataWithFilter(type) {
    this.pokemonSrv.getPokemonListWithType(type).subscribe((res) => {
      this.masterPokemonList = res.pokemon;
      this.initPaginationData();
    },err=>{
      this.gs.showToast("Something Went Wrong")
    });
  }

  fetchMore(event) {
    this.pagination.offset += 20;
    this.initPaginationData();
    setTimeout(() => {
      event.target.complete();
    }, 1500);
  }

  // Manual Pagination for Pokemon data list (filter by type)
  initPaginationData() {
    const data = this.masterPokemonList.slice(this.pagination.offset, this.pagination.limit + this.pagination.offset);
    data.forEach((pokemon) => {
      // Split from URL to get pokemon ID
      const urlSplit = pokemon.pokemon.url.split("/");
      const id = urlSplit[urlSplit.length - 2];
      // String ID : 001, for favorites
      pokemon.pokemon.stringId = id.toString().padStart(3, "0");
      this.pokemonSrv.getPokemonDetail(id).subscribe((resDetail) => {
        pokemon.pokemon.detail = resDetail;
      });
      this.pokemonList.push(pokemon.pokemon);
    });
  }
}
