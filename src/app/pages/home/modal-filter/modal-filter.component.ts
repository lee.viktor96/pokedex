import { Component, Input, OnInit } from "@angular/core";
import { ModalController } from "@ionic/angular";
import { GlobalService } from "@shared/services";
import { PokemonService } from "@shared/services/pokemon.service";

@Component({
  selector: "app-modal-filter",
  templateUrl: "./modal-filter.component.html",
  styleUrls: ["./modal-filter.component.scss"],
})
export class ModalFilterComponent implements OnInit {
  pokemonTypes;
  selectedType;
  @Input("currentFilter") currentFilter;
  constructor(private pokemonSrv: PokemonService, private modalController: ModalController, private gs:GlobalService) {}

  ngOnInit() {
    this.selectedType = this.currentFilter;
    // Fetch pokemon types Once only from API
    if(this.pokemonSrv.getLocalPokemonType()){
      this.pokemonTypes = this.pokemonSrv.getLocalPokemonType()
    }else{
      this.pokemonSrv.getPokemonType().subscribe((res) => {
        this.pokemonTypes = res.results;
        this.pokemonSrv.setLocalPokemonTypes(this.pokemonTypes)
      },err=>{
        this.gs.showToast("Something Went Wrong")
      });
    }
  }

  dismissModal() {
    this.modalController.dismiss(this.selectedType);
  }

  selectItem(item) {
    if (this.selectedType && this.selectedType.name === item.name) {
      this.selectedType = null;
    } else {
      this.selectedType = item;
    }
  }
}
